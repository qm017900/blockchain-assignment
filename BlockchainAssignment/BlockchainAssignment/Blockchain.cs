﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    class Blockchain
    {
        public List<Block> Blocks = new List<Block>(); // the list of blocks (basically the blockchain) 

        public List<Transaction> pendingTransactions = new List<Transaction>(); // list of transactions for the blocks to choose from 
        int transactionsPerBlock = 5;// number of transactions each block can hold 

        public Blockchain()
        {
            Blocks.Add(new Block()); // adding block to the block chain 
        }

        public String getBlockAsString(int index)
        {
            return Blocks[index].ToString(); // returning the block as a string for printing on the screen 
        }

        public Block GetLastBlock()
        {
            return Blocks[Blocks.Count - 1]; // getting the last block important for getting the previous hash 
        }

        public List<Transaction> getPendingTransactions() // picks transactions arbitarily 
        {
            int n = Math.Min(transactionsPerBlock, pendingTransactions.Count);
            List<Transaction> transactions = pendingTransactions.GetRange(0, n);
            pendingTransactions.RemoveRange(0, n);

            return transactions;
        }

        public List<Transaction> TransactionsGreedy() // picks the trainsactions based on on greed 
        {
            int n = Math.Min(transactionsPerBlock, pendingTransactions.Count);
            List<Transaction> transactions = pendingTransactions.GetRange(0, n);
            List<Transaction> SortedList = transactions.OrderBy(t => t.amount).ToList();// sorts by amount so the block picks the transactions worth the most 
            List<Transaction> ShortendList = new List<Transaction>();
            if (transactions.Count <= 5) // 5 is chosen here ias it is outlines earlier as the amount of transactions a block can hold 
            {
                 ShortendList = SortedList;
            }else
            {
                 ShortendList = SortedList.GetRange(0, 5); // this if statement makes sure the list can be sent now matter the size 
            }
            pendingTransactions.RemoveRange(0, n);
            return ShortendList;
        }

        public List<Transaction> TransactionsAltruistic()// picks transactions based on how long they have been waiting 
        {
            int n = Math.Min(transactionsPerBlock, pendingTransactions.Count);
            List<Transaction> transactions = pendingTransactions.GetRange(0, n);
            List<Transaction> SortedList = transactions.OrderByDescending(t => t.Time()).ToList();// gets the datetime from transactions and sorts the blockchain based on it 
            List<Transaction> ShortendList = new List<Transaction>();
            if (transactions.Count <= 5)
            {
                ShortendList = SortedList;
            }
            else
            {
                ShortendList = SortedList.GetRange(0, 5); // again if statement to handle diffirent list sizes 
            }
            pendingTransactions.RemoveRange(0, n);
            return ShortendList;
        }

        public List<Transaction> TransactionsUnpredictable()// randomly picks transactions 
        {
            int n = Math.Min(transactionsPerBlock, pendingTransactions.Count);
            List<Transaction> transactions = pendingTransactions.GetRange(0, n);
            var random = new Random();
            List<Transaction> traClone = new List<Transaction>(transactions);// makes a clone so i can remove elements when they are picked 
            List<Transaction> SortedList = new List<Transaction>();
            if (traClone.Count <= 5)
            {
                for (int x = 0; x <= traClone.Count; x++)
                {
                    int index = random.Next(0, traClone.Count());
                    SortedList.Add(traClone[index]);
                    traClone.RemoveAt(index);
                }
            }
            else
            {
                for (int x = 0; x <= 5; x++)
                {
                    int index = random.Next(traClone.Count);
                    SortedList.Add(traClone[index]);
                    traClone.RemoveAt(index);
                }
            }



            pendingTransactions.RemoveRange(0, n);
            return SortedList;
        }

        public List<Transaction> TransactionsAddress(Block lastBlock)
        {
           
            int n = Math.Min(transactionsPerBlock, pendingTransactions.Count);
            List<Transaction> transactions = pendingTransactions.GetRange(0, n);
            List<Transaction> SortedList = new List<Transaction>();
            for (int x = 0; x <= transactions.Count(); x++){
                if (lastBlock.hash == transactions[x].senderAddress)
                {
                    SortedList.Add(transactions[x]);
                    transactions.RemoveAt(x);
                }
            }

            n = Math.Min(transactionsPerBlock, transactions.Count);
            List<Transaction> transactions2 = transactions.GetRange(0, n);
            SortedList.AddRange(transactions2);
            transactions.RemoveRange(0, n);
            return SortedList;
        }
        public override string ToString()
        {
            String output = String.Empty;
            Blocks.ForEach(b => output += (b.ToString() + "\n"));
            return output;
        }

        public double GetBalance(String address)
        {
            double balance = 0.0;
            foreach(Block b in Blocks)
            {
                foreach(Transaction t in b.transactionList)
                {
                    if (t.recipientAddress.Equals(address))
                    {
                        balance += t.amount;
                    }
                    if (t.senderAddress.Equals(address))
                    {
                        balance -= (t.amount + t.fee);
                    }
                }
            }

            return balance;
        }

        public bool validateMerkleRoot(Block b)
        {
            String reMerkle = Block.MerkleRoot(b.transactionList);
            return reMerkle.Equals(b.merkleRoot);
        }
    }
}
