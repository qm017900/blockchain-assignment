﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlockchainAssignment
{
    public partial class BlockchainApp : Form
    {
        Blockchain blockchain;

        public BlockchainApp()// creates genesis block
        {
            InitializeComponent();
            blockchain = new Blockchain();

            // Message printed when initialised
            richTextBox1.Text = "New Blockchain Initialised!";
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e) // prints block at indec 
        {
            int index = 0;
            if(Int32.TryParse(textBox1.Text, out index))// you can enter index
            {
                richTextBox1.Text = blockchain.getBlockAsString(index);
            }
        }
        

        private void button2_Click(object sender, EventArgs e)// creates a wallet 
        {
            String privKey;
            Wallet.Wallet myNewWallet = new Wallet.Wallet(out privKey);
            textBox2.Text = myNewWallet.publicID;
            textBox3.Text = privKey;
        }


        /* Validate Keys */
        private void button3_Click(object sender, EventArgs e)// validates the keys 
        {
            if(Wallet.Wallet.ValidatePrivateKey(textBox3.Text, textBox2.Text))
            {
                richTextBox1.Text = "Keys are valid!";
            }
            else
            {
                richTextBox1.Text = "Keys are not valid";
            }
        }

        private void button4_Click(object sender, EventArgs e)// creates a new transaction 
        {
            Transaction transaction = new Transaction(textBox2.Text, textBox4.Text, Double.Parse(textBox5.Text), Double.Parse(textBox6.Text), textBox3.Text);
            blockchain.pendingTransactions.Add(transaction);
            richTextBox1.Text = transaction.ToString();
        }

        private void button5_Click(object sender, EventArgs e)// adds a new block to the blockchain
        {
            List<Transaction> transactions = blockchain.getPendingTransactions();
            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, textBox2.Text);
            blockchain.Blocks.Add(newBlock);

            richTextBox1.Text = blockchain.getBlockAsString(newBlock.index);
        }

        private void button6_Click(object sender, EventArgs e)// prints the block chain to the screen 
        {
            richTextBox1.Text = blockchain.ToString();
        }

        private void button7_Click(object sender, EventArgs e)// prints pending transactions 
        {
            String s = String.Empty;
            List<Transaction> transactions = blockchain.getPendingTransactions();
            s = string.Join(",", transactions);
            richTextBox1.Text = s;
        }
        private void button8_Click(object sender, EventArgs e)// gets the balance of the transactions  
        {
            richTextBox1.Text = blockchain.GetBalance(textBox2.Text).ToString() + " Tom Bucks";
        }

        private void button9_Click(object sender, EventArgs e)// validaties the blockchain 
        {
            if (blockchain.Blocks.Count == 1)
            {
                if (!blockchain.validateMerkleRoot(blockchain.Blocks[0]))
                {
                    richTextBox1.Text = "Blockchain is invalid";
                }
                else
                {
                    richTextBox1.Text = "Blockchain is valid";
                }
                return;
            }
            bool valid = true;
            for (int i = 1; i < blockchain.Blocks.Count - 1; i++)
            {
                if (blockchain.Blocks[i].prevHash != blockchain.Blocks[i - 1].hash || !blockchain.validateMerkleRoot(blockchain.Blocks[i]))
                {
                    richTextBox1.Text = "Blockchain is invalid";
                    return;
                }
            }
            if (valid)
            {
                richTextBox1.Text = "Blockchain is valid";
            }
            else
            {
                richTextBox1.Text = "Blockchain is invalid";
            }
        }

        private void button10_Click(object sender, EventArgs e)// creates an invalid block for testing purposes 
        {
            List<Transaction> transactions = blockchain.getPendingTransactions();
            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, 1, textBox2.Text);
            blockchain.Blocks.Add(newBlock);

            richTextBox1.Text = blockchain.getBlockAsString(newBlock.index);
        }

        private void button11_Click(object sender, EventArgs e)// creates a new block but selects the transactions greedily 
        {
            List<Transaction> transactions = blockchain.TransactionsGreedy();
            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, textBox2.Text);
            blockchain.Blocks.Add(newBlock);

            richTextBox1.Text = blockchain.getBlockAsString(newBlock.index);
        }

        private void button12_Click(object sender, EventArgs e)// creates a new block but selects the transactions based on how old they are 
        {
            List<Transaction> transactions = blockchain.TransactionsAltruistic();
            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, textBox2.Text);
            blockchain.Blocks.Add(newBlock);

            richTextBox1.Text = blockchain.getBlockAsString(newBlock.index);
        }

        private void button13_Click(object sender, EventArgs e)// creates a new block but selects the transactions randomly 
        {
            List<Transaction> transactions = blockchain.TransactionsUnpredictable();
            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, textBox2.Text);
            blockchain.Blocks.Add(newBlock);

            richTextBox1.Text = blockchain.getBlockAsString(newBlock.index);
        }

        private void button14_Click(object sender, EventArgs e)// creates a new block but selects the transactions based on personal address
        {
            List<Transaction> transactions = blockchain.TransactionsAddress(blockchain.GetLastBlock());
            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, textBox2.Text);
            blockchain.Blocks.Add(newBlock);

            richTextBox1.Text = blockchain.getBlockAsString(newBlock.index);
        }
    }
}
